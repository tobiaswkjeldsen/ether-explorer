import 'dart:async';
import 'package:etherstats/block_details_page.dart';
import 'package:etherstats/block_row.dart';
import 'package:etherstats/etherchain_api.dart';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:etherstats/transaction_details_page.dart';
import 'package:etherstats/transaction_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TransactionsPage extends StatefulWidget {
  TransactionsPage({Key key, this.basicStats}) : super(key: key);

  final BasicStats basicStats;

  @override
  _TransactionsPageState createState() => new _TransactionsPageState(basicStats);
}

class _TransactionsPageState extends State<TransactionsPage> {
  BasicStats _basicStats;

  EtherChainApi _etherChainApi = new EtherChainApi();

  List<Transaction> _transactions = new List<Transaction>();
  int transactionsChunkSize = 50;
  int transactionsNextChunk = 0;

  _TransactionsPageState(basicStats) {
    _fetchData();
    this._basicStats = basicStats;
  }

  Future<Null>_fetchData([bool reset = false]) {
    return _etherChainApi.getTransactions(transactionsNextChunk, transactionsChunkSize).then((transactions){
      setState(() {
        if (reset) {
          _transactions.clear();
        }
        _transactions.addAll(transactions);
      });
      transactionsNextChunk += transactionsChunkSize;
    });
  }

  void _onTransactionPressed(BuildContext context, transaction) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new TransactionDetails(transaction: transaction, basicStats: _basicStats,);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Transactions'),
      ),
      body: new RefreshIndicator(
          onRefresh: () => _fetchData(true),
          child: new ListView.builder(
            itemExtent: TransactionRow.kHeight,
            itemCount: _transactions.length,
            itemBuilder: (BuildContext context, int index) {
              if (index == _transactions.length - 1) {
                _fetchData();
              }
              return new TransactionRow(
                transaction: _transactions[index],
                onPressed: (transaction) {
                  _onTransactionPressed(context, transaction);
                },
              );
            },
          )),
    );
  }
}

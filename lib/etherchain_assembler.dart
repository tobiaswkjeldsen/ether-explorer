import 'package:intl/intl.dart';

class EtherChainResult {
  final int status;
  final data;

  EtherChainResult(this.status, this.data);

  static EtherChainResult empty() {
    return new EtherChainResult(null, null);
  }

  get(field) {
    if (data != null && data[field][field] != null) {
      return data[field][field];
    } else {
      return "-";
    }
  }
}

class BasicStats {
  String difficulty = '-';
  String blockTime;
  String uncleRatePercent;
  String hashRate;
  double priceUsd;
  double priceBtc;
  double transactionsPerSecond;

  List<Transaction> transactions;
  List<Block> blocks;

  BasicStats(
      {this.difficulty: '-',
      this.blockTime: '-',
      this.uncleRatePercent: '-',
      this.hashRate = '-',
      this.priceUsd: .0,
      this.priceBtc: .0,
      this.transactionsPerSecond: .0,
      this.transactions,
      this.blocks});

  static BasicStats empty() {
    BasicStats basicStats = new BasicStats();
    basicStats.transactions = new List<Transaction>();
    basicStats.blocks = new List<Block>();
    return basicStats;
  }
}

class Block {
  num number;
  String hash;
  String parentHash;
  String uncleHash;
  String rootHash;
  String txHash;
  String coinBase;
  num size;
  num difficulty;
  num gasLimit;
  num gasUsage;
  DateTime time;
  String miner;
  num reward;

  Block(
      {this.number: .0,
      this.hash: '-',
      this.parentHash: '-',
      this.uncleHash: '-',
      this.rootHash: '-',
      this.txHash: '-',
      this.coinBase: '-',
      this.size: .0,
      this.difficulty: .0,
      this.gasLimit: .0,
      this.gasUsage: .0,
      this.time,
      this.miner: '-',
      this.reward: .0});
}

class Transaction {
  String hash;
  String sender;
  String recipient;
  num amount;
  num gasPrice;
  num gasLimit;
  num gasUsed;
  String type;
  num blockId;
  String blockHash;
  String parentHash;
  DateTime time;

  Transaction(
      {this.hash,
      this.sender: '-',
      this.recipient: '-',
      this.amount: .0,
      this.gasPrice: .0,
      this.gasLimit: .0,
      this.gasUsed: .0,
      this.type,
      this.blockId: 0,
      this.blockHash: '-',
      this.parentHash: '-',
      this.time});
}

class Account {
  String address;
  num balance;
  String code;
  DateTime firstSeen;

  Account({this.address: '-', this.balance: .0, this.code: '-', this.firstSeen});
}

class EtherChainAssembler {
  var dateFormatter = new DateFormat('yyyy-mm-dTh:m:s.000Z');

  BasicStats basicStats(Map json) {
    List txs = json['txs'];
    List<Transaction> transactions = new List<Transaction>();
    txs.forEach((element) {
      transactions.add(new Transaction(
          hash: element['hash'],
          sender: element['sender'],
          recipient: element['recipient'],
          amount: element['amount'],
          time: DateTime.parse(element['time'])));
    });
    List blks = json['blocks'];
    List<Block> blocks = new List<Block>();
    blks.forEach((element) {
      blocks.add(new Block(
          number: element['number'],
          miner: element['name'],
          time: DateTime.parse(element['time']),
          coinBase: element['coinbase']));
    });
    return new BasicStats(
        difficulty:
            '${(json['stats']['difficulty']/1000000000000).toStringAsPrecision(6)} TH',
        blockTime:
            '${json['stats']['blockTime'].toStringAsPrecision(2)} seconds',
        uncleRatePercent:
            '${(json['stats']['uncle_rate']*100).toStringAsPrecision(3)} %',
        hashRate:
            '${(json['stats']['hashRate']/1000000000000).toStringAsPrecision(5)} TH/s',
        priceUsd: json['price']['usd'],
        priceBtc: json['price']['btc'],
        transactionsPerSecond: json['txCount']['count'],
        transactions: transactions,
        blocks: blocks);
  }

  Transaction transaction(json) {
    return new Transaction(
        hash: json['hash'],
        sender: json['sender'],
        recipient: json['recipient'],
        amount: json['amount'],
        gasPrice: json['price'],
        gasLimit: json['gasLimit'],
        gasUsed: json['gasUsed'],
        type: json['type'],
        blockHash: json['blockHash'],
        blockId: json['block_id'],
        parentHash: json['parentHash'],
        time: DateTime.parse(json['time']));
  }

  List<Transaction> transactions(List json) {
    List<Transaction> transactions = new List<Transaction>();
    json.forEach((element) {
      transactions.add(transaction(element));
    });
    return transactions;
  }

  Block block(json) {
    return new Block(
        number: json['number'],
        hash: json['hash'],
        parentHash: json['parentHash'],
        uncleHash: json['uncleHash'],
        rootHash: json['root'],
        txHash: json['txHash'],
        coinBase: json['coinbase'],
        size: json['size'],
        difficulty: json['difficulty'],
        gasLimit: json['gasLimit'],
        gasUsage: json['gasUsed'],
        time: DateTime.parse(json['time']),
        reward: json['reward']);
  }

  List<Block> blocks(List json) {
    List<Block> blocks = new List<Block>();
    json.forEach((element) {
      blocks.add(block(element));
    });
    return blocks;
  }

  Account account(json) {
    return new Account(address: json['address'], balance: json['balance'], code: json['code'], firstSeen: DateTime.parse(json['firstSeen']));
  }
}

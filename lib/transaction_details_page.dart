import 'package:etherstats/account_details_page.dart';
import 'package:etherstats/block_details_page.dart';
import 'package:etherstats/date_helper.dart';
import 'package:etherstats/etherchain_api.dart';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:flutter/material.dart';

class _TransactionDetailsState extends State<TransactionDetails> {
  var _etherChainApi = new EtherChainApi();

  Transaction _transaction;
  BasicStats _basicStats;
  Transaction _transactionDetails = new Transaction();

  _TransactionDetailsState(transaction, basicStats) {
    this._transaction = transaction;
    this._basicStats = basicStats;

    _fetchTransactionDetails();
  }

  void _fetchTransactionDetails() {
    _etherChainApi.getTransaction(_transaction.hash).then((transaction) {
      setState(() {
        _transactionDetails = transaction;
      });
    });
  }

  void _onBlockPressed(BuildContext context, block) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new BlockDetails(block: block, basicStats: _basicStats,);
      },
    ));
  }

  void _onAccountPressed(BuildContext context, account) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new AccountDetails(account: account, basicStats: _basicStats,);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Transaction details"),
      ),
      body: new ListView(
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 2.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Transaction hash',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${_transaction.hash}',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 12.0,
                          fontFamily: 'Monospace',
                          fontWeight: FontWeight.w300),
                    )
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Time',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${DateHelper.date(_transaction.time)} (${DateHelper.minutesOrHoursOrDaysAgo(_transaction.time)})',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 19.0,
                          fontWeight: FontWeight.w300),
                    )
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Amount',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${(_transactionDetails.amount/1000000000000000000).toStringAsFixed(8)} Ξ'
                          '\n${((_transactionDetails.amount/1000000000000000000)*_basicStats.priceUsd).toStringAsFixed(2)} USD',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 19.0,
                          fontWeight: FontWeight.w300),
                    )
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new InkWell(
                    onTap: () => _onBlockPressed(context,
                        new Block(number: _transactionDetails.blockId)),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text('Block',
                            style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w400,
                            )),
                        new Text(
                          '#${_transactionDetails.blockId}',
                          style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 19.0,
                              fontWeight: FontWeight.w300),
                        )
                      ],
                    )),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new InkWell(
                    onTap: () => _onAccountPressed(context,
                        new Account(address: _transactionDetails.sender)),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text('From',
                            style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w400,
                            )),
                        new Text(
                          '${_transactionDetails.sender}',
                          style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 12.0,
                              fontFamily: 'Monospace',
                              fontWeight: FontWeight.w300),
                        )
                      ],
                    )),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new InkWell(
                    onTap: () => _onAccountPressed(context,
                        new Account(address: _transactionDetails.recipient)),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text('To',
                            style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w400,
                            )),
                        new Text(
                          '${_transactionDetails.recipient}',
                          style: new TextStyle(
                              color: Colors.black54,
                              fontSize: 12.0,
                              fontFamily: 'Monospace',
                              fontWeight: FontWeight.w300),
                        )
                      ],
                    )),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Tx price',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${(_transactionDetails.gasUsed*_transactionDetails.gasPrice/1000000000000000000).toStringAsFixed(8)} Ξ',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 19.0,
                          fontWeight: FontWeight.w300),
                    )
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Gas price',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${(_transactionDetails.gasPrice/1000000000).toStringAsFixed(0)} GWei',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 19.0,
                          fontWeight: FontWeight.w300),
                    )
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Gas limit',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${(_transactionDetails.gasLimit/1000).toStringAsFixed(3)}',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 19.0,
                          fontWeight: FontWeight.w300),
                    )
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Total gas used',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${(_transactionDetails.gasUsed/1000).toStringAsFixed(3)}',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 19.0,
                          fontWeight: FontWeight.w300),
                    )
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class TransactionDetails extends StatefulWidget {
  TransactionDetails({Key key, this.transaction, this.basicStats}) : super(key: key);

  final Transaction transaction;
  final BasicStats basicStats;

  @override
  _TransactionDetailsState createState() =>
      new _TransactionDetailsState(transaction, basicStats);
}

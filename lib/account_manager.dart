import 'dart:async';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:etherstats/preferences_manager.dart';

class AccountManager {
  PreferencesManager _preferencesManager = new PreferencesManager();

  Future<List<Account>> getAccounts() {
    return _preferencesManager.getAccounts();
  }

  Future<Null> addAccount(address) {
    return _preferencesManager.getAccounts().then((accounts) {
      accounts.add(new Account(address: address));
      _preferencesManager.saveAccounts(accounts);
    });
  }

  Future<Null> removeAccount(address) {
    return _preferencesManager.getAccounts().then((accounts) {
      accounts.removeWhere((account) => account.address == address);
      _preferencesManager.saveAccounts(accounts);
    });
  }
}
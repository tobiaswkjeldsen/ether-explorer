import 'package:etherstats/etherchain_assembler.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

typedef void TransactionRowActionCallback(Transaction transaction);

class TransactionRow extends StatelessWidget {
  final Transaction transaction;
  final bool received;
  final bool showDirection;
  final TransactionRowActionCallback onPressed;

  TransactionRow({Key key, this.transaction, this.showDirection: false, this.received: false, this.onPressed})
      : super(key: key);

  static const double kHeight = 60.0;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
        onTap: () {
          onPressed(transaction);
        },
        child: new Container(
            decoration: new BoxDecoration(
                border: new Border(
                    bottom:
                        new BorderSide(color: Theme.of(context).dividerColor))),
            padding: const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
            child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  new Text(
                    '${transaction.hash.substring(0, 10)}',
                    style: new TextStyle(
                      color: Colors.black54,
                      fontSize: 12.0,
                      fontFamily: 'Monospace',
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  new Text(
                    '${transaction.sender.substring(0, 10)}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 12.0,
                        fontFamily: 'Monospace',
                        fontWeight: FontWeight.w300),
                  ),
                  new Text(
                    '${transaction.recipient.substring(0, 10)}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 12.0,
                        fontFamily: 'Monospace',
                        fontWeight: FontWeight.w300),
                  ),
                  new Text(
                    '${(transaction.amount / 1000000000000000000)
                      .toStringAsFixed(3)} Ξ',
                    style: new TextStyle(
                        color: showDirection ? received ? Colors.green : Colors.red : Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  ),
                ])));
  }
}

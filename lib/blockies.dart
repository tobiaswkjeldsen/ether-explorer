import 'dart:math';
import 'dart:ui';

class Opts {
  Opts(
      {this.size,
      this.scale,
      this.color,
      this.spotcolor,
      this.bgcolor,
      this.seed});

  final int size;
  final int scale;
  final List color;
  final List bgcolor;
  final List spotcolor;
  final seed;
}

class Blockies {
  var randseed = new List(4); // Xorshift: [x, y, z, w] 32 bit values

  void seedrand(String seed) {
    for (var i = 0; i < randseed.length; i++) {
      randseed[i] = 0;
    }
    for (var i = 0; i < seed.length; i++) {
      randseed[i % 4] =
          (randseed[i % 4] << 5) - randseed[i % 4] + seed.codeUnitAt(i);
    }
  }

  num rand() {
    // based on Java's String.hashCode(), expanded to 4 32bit values
    var t = randseed[0] ^ (randseed[0] << 11);

    randseed[0] = randseed[1];
    randseed[1] = randseed[2];
    randseed[2] = randseed[3];
    randseed[3] = randseed[3] ^ (randseed[3] >> 19) ^ t ^ (t >> 8);

    return (randseed[3] >> 0) / (1 << 31 >> 0);
  }

  List createColor() {
    //saturation is the whole color spectrum
    var h = (rand() * 360).floor();
    //saturation goes from 40 to 100, it avoids greyish colors
    var s = rand() * 60 + 40;
    //lightness can be anything from 0 to 100, but probabilities are a bell curve around 50%
    var l = (rand() + rand() + rand() + rand()) * 25;

    return [h / 360, s / 100, l / 100];
  }

  List createImageData(size) {
    var width = size; // Only support square icons for now
    var height = size;

    int dataWidth = (width / 2).ceil();
    int mirrorWidth = width - dataWidth;

    var data = [];
    for (var y = 0; y < height; y++) {
      var row = new List();
      for (var x = 0; x < dataWidth; x++) {
        // this makes foreground and background color to have a 43% (1/2.3) probability
        // spot color has 13% chance
        row.add((rand() * 2.3).floor());
      }
      Iterable r = row.getRange(0, mirrorWidth);
      r = r.toList().reversed;
      row.addAll(r);

      for (var i = 0; i < row.length; i++) {
        data.add(row[i]);
      }
    }

    return data;
  }

  Opts buildOpts(seed) {
    seedrand(seed);
    Opts opts = new Opts(
        size: 8,
        scale: 16,
        color: createColor(),
        bgcolor: createColor(),
        spotcolor: createColor(),
        seed: seed);


    return opts;
  }

  hue2rgb(p, q, t) {
    if (t < 0) t += 1;
    if (t > 1) t -= 1;
    if (t < 1 / 6) return p + (q - p) * 6 * t;
    if (t < 1 / 2) return q;
    if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
    return p;
  }

  List hsl2rgb(h, s, l) {
    var r, g, b;

    if (s == 0) {
      r = g = b = l; // achromatic
    } else {
      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      var p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }

    return [(r * 255).round(), (g * 255).round(), (b * 255).round(), 255];
  }

  Canvas toCanvas(String address) {
    Opts opts = buildOpts(address.toLowerCase());

    var imageData = createImageData(opts.size);
    var width = sqrt(imageData.length);

    //const p = new PNG(opts.size*opts.scale, opts.size*opts.scale, 3)
    Canvas canvas = new Canvas(
        new PictureRecorder(),
        new Rect.fromLTWH(
            .0, .0, opts.size * opts.scale.toDouble(), opts.size * opts.scale.toDouble()));
    //const bgcolor = p.color(...hsl2rgb(...opts.bgcolor))
    Paint bgcolor = new Paint();
    var rgb = hsl2rgb(opts.bgcolor[0], opts.bgcolor[1], opts.bgcolor[2]);
    bgcolor.color = new Color.fromARGB(255, rgb[0], rgb[1], rgb[2]);

    Paint color = new Paint();
    rgb = hsl2rgb(opts.color[0], opts.color[1], opts.color[2]);
    color.color = new Color.fromARGB(255, rgb[0], rgb[1], rgb[2]);

    Paint spotcolor = new Paint();
    rgb = hsl2rgb(opts.spotcolor[0], opts.spotcolor[1], opts.spotcolor[2]);
    color.color = new Color.fromARGB(255, rgb[0], rgb[1], rgb[2]);

    for (var i = 0; i < imageData.length; i++) {
      var row = (i / width).floor();
      var col = i % width;
      // if data is 0, leave the background
      if (imageData[i] > 0) {
        // if data is 2, choose spot color, if 1 choose foreground
        //var pngColor = imageData[i] == 1 ? color : spotcolor;
        //p.fillRect(col * opts.scale, row * opts.scale, opts.scale, opts.scale, pngColor);

        var paint = imageData[i] == 1 ? color : spotcolor;
        canvas.drawRect(
            new Rect.fromLTWH(
                col * opts.scale, row * opts.scale.toDouble(), opts.scale.toDouble(), opts.scale.toDouble()),
            paint);
      }
    }
    return canvas;
  }
}

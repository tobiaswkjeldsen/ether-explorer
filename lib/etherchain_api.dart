import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:etherstats/etherchain_assembler.dart';

class EtherChainApi {
  var _httpClient = createHttpClient();
  var _etherChainAssembler = new EtherChainAssembler();

  Future<BasicStats> getBasicStats() {
    return _httpClient.get('https://etherchain.org/api/basic_stats').then((response){
      return _etherChainAssembler.basicStats(JSON.decode(response.body)['data']);
    });
  }

  Future<List<Transaction>> getTransactions(offset, chunkSize) {
    return _httpClient.get('https://etherchain.org/api/txs/${offset}/${chunkSize}').then((response){
      return _etherChainAssembler.transactions(JSON.decode(response.body)['data']);
    });
  }

  Future<List<Block>> getBlocks(offset, chunkSize) {
    return _httpClient.get('https://etherchain.org/api/blocks/${offset}/${chunkSize}').then((response){
      return _etherChainAssembler.blocks(JSON.decode(response.body)['data']);
    });
  }

  Future<Transaction> getTransaction(transactionHash) {
    return _httpClient.get('https://etherchain.org/api/tx/$transactionHash').then((response){
      return _etherChainAssembler.transaction(JSON.decode(response.body)['data'][0]);
    });
  }

  Future<Block> getBlock(blockId) {
    return _httpClient.get('https://etherchain.org/api/block/$blockId').then((response){
      return _etherChainAssembler.block(JSON.decode(response.body)['data'][0]);
    });
  }

  Future<Account> getAccount(address) {
    return _httpClient.get('https://etherchain.org/api/account/$address').then((response){
      return _etherChainAssembler.account(JSON.decode(response.body)['data'][0]);
    });
  }

  Future<List<Transaction>> getAccountTransactions(address) {
    return _httpClient.get('https://etherchain.org/api/account/$address/tx/0').then((response){
      return _etherChainAssembler.transactions(JSON.decode(response.body)['data']);
    });
  }

}
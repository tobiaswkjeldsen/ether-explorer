import 'package:etherstats/preferences_manager.dart';
import 'package:etherstats/setting_types.dart';
import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  const Settings(this.configuration, this.updater);

  final Setting configuration;
  final ValueChanged<Setting> updater;

  @override
  SettingsState createState() => new SettingsState();
}

class SettingsState extends State<Settings> {
  var _preferencesManager = new PreferencesManager();

  void _handleThemeChanged(ThemeColor color) {
    sendUpdates(widget.configuration.copyWith(theme: color));
    _preferencesManager.saveTheme(color.index);
    Navigator.pop(context, true);
  }

  void _changeTheme() {
    showDialog<bool>(
      context: context,
      child: new SimpleDialog(title: const Text("Color"), children: [
        new ListTile(
            title: new Text("Blue"),
            onTap: () {
              _handleThemeChanged(ThemeColor.blue);
            }),
        new ListTile(
            title: new Text("Blue grey"),
            onTap: () {
              _handleThemeChanged(ThemeColor.blueGrey);
            }),
        new ListTile(
            title: new Text("Grey"),
            onTap: () {
              _handleThemeChanged(ThemeColor.grey);
            }),
        new ListTile(
            title: new Text("Green"),
            onTap: () {
              _handleThemeChanged(ThemeColor.green);
            }),
        new ListTile(
            title: new Text("Red"),
            onTap: () {
              _handleThemeChanged(ThemeColor.red);
            }),
        new ListTile(
            title: new Text("Purple"),
            onTap: () {
              _handleThemeChanged(ThemeColor.purple);
            }),
      ]),
    );
  }

  void sendUpdates(Setting value) {
    if (widget.updater != null) widget.updater(value);
  }

  Widget buildSettingsPane(BuildContext context) {
    final List<Widget> rows = <Widget>[
      new ListTile(
        leading: new Icon(Icons.color_lens),
        title: const Text('Color'),
        onTap: _changeTheme,
      ),
    ];
    return new ListView(
      padding: const EdgeInsets.symmetric(vertical: 20.0),
      children: rows,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(title: const Text('Settings')),
        body: buildSettingsPane(context));
  }
}

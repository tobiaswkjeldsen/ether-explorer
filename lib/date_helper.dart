import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class DateHelper {
  static DateFormat _formatHourMinutes = new DateFormat('HH:mm');
  static DateFormat _formatDate = new DateFormat('yyyy-MM-dd HH:mm:ss');

  static initLocale() {
    Intl.defaultLocale = 'da_DK';
    initializeDateFormatting('da_DK');
  }

  static date(DateTime dateTime) {
    return _formatDate.format(dateTime?? new DateTime.now());
  }

  static minutesOrHoursOrDaysAgo(DateTime dateTime) {
    int differenceMinutes = new DateTime.now().difference(dateTime).inMinutes;
    if(differenceMinutes > 1440) {
      return '${new DateTime.now().difference(dateTime).inDays} days ago';
    } else if(differenceMinutes > 60) {
      return '${new DateTime.now().difference(dateTime).inHours} hours ago';
    } else if(differenceMinutes > 1) {
      return '${new DateTime.now().difference(dateTime).inMinutes} minutes ago';
    } else if(differenceMinutes == 1) {
      return '${new DateTime.now().difference(dateTime).inMinutes} minute ago';
    } else {
      return 'just now';
    }
  }

  static minutesAgo(DateTime dateTime) {
    return '${new DateTime.now().difference(dateTime).inMinutes} minutes ago';
  }

  static secondsAgo(DateTime dateTime) {
    return '${new DateTime.now().difference(dateTime).inSeconds} seconds ago';
  }

}

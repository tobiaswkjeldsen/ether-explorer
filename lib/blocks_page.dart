import 'dart:async';
import 'package:etherstats/block_details_page.dart';
import 'package:etherstats/block_row.dart';
import 'package:etherstats/etherchain_api.dart';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BlocksPage extends StatefulWidget {
  BlocksPage({Key key, this.basicStats}) : super(key: key);

  final BasicStats basicStats;

  @override
  _BlocksPageState createState() => new _BlocksPageState(basicStats);
}

class _BlocksPageState extends State<BlocksPage> {
  EtherChainApi _etherChainApi = new EtherChainApi();

  BasicStats _basicStats;

  List<Block> _blocks = new List<Block>();
  int blocksChunkSize = 50;
  int blocksNextChunk = 0;

  _BlocksPageState(basicStats) {
    _fetchData();
    this._basicStats = basicStats;
  }

  Future<Null>_fetchData([bool reset = false]) {
    return _etherChainApi.getBlocks(blocksNextChunk, blocksChunkSize).then((blocks){
      setState(() {
        if (reset) {
          _blocks.clear();
        }
        _blocks.addAll(blocks);
      });
      blocksNextChunk += blocksChunkSize;
    });
  }

  void _onBlockPressed(BuildContext context, block) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new BlockDetails(block: block, basicStats: _basicStats,);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Blocks'),
      ),
      body: new RefreshIndicator(
          onRefresh: () => _fetchData(true),
          child: new ListView.builder(
            itemExtent: BlockRow.kHeight,
            itemCount: _blocks.length,
            itemBuilder: (BuildContext context, int index) {
              if (index == _blocks.length - 1) {
                _fetchData();
              }
              return new BlockRow(
                block: _blocks[index],
                onPressed: (block) {
                  _onBlockPressed(context, block);
                },
              );
            },
          )),
    );
  }
}

import 'package:etherstats/account_details_page.dart';
import 'package:etherstats/date_helper.dart';
import 'package:etherstats/etherchain_api.dart';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:flutter/material.dart';

class _BlockDetailsState extends State<BlockDetails> {
  var _etherChainApi = new EtherChainApi();

  Block _block;
  BasicStats _basicStats;
  Block _blockDetails = new Block();

  _BlockDetailsState(block, basicStats) {
    this._block = block;
    this._basicStats = basicStats;

    _fetchBlockDetails();
  }

  void _fetchBlockDetails() {
    _etherChainApi.getBlock(_block.number).then((block) {
      setState(() {
        _blockDetails = block;
      });
    });
  }

  void _onAccountPressed(BuildContext context, account) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new AccountDetails(account: account, basicStats: _basicStats,);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Block details"),
      ),
      body: new ListView(children: <Widget>[
        new Container(
          padding: const EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 2.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Block hash',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${_blockDetails.hash}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 12.0,
                        fontFamily: 'Monospace',
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),

              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Time',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${DateHelper.date(_block.time?? new DateTime.now())} (${DateHelper.minutesOrHoursOrDaysAgo(_block.time?? new DateTime.now())})',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Block',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '#${_blockDetails.number}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  ),
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Reward',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${(_blockDetails.reward/1000000000000000000).toStringAsFixed(8)} Ξ',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  ),
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Difficulty',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${_blockDetails.difficulty}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new InkWell(
                onTap: () => _onAccountPressed(context, new Account(address: _blockDetails.coinBase)),
                  child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Miner',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${_block.miner}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  ),
                  new Text(
                    '${_blockDetails.coinBase}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 12.0,
                        fontFamily: 'Monospace',
                        fontWeight: FontWeight.w300),
                  )
                ],
              )),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Gas limit',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${_blockDetails.gasLimit}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Gas usage',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${((_blockDetails.gasUsage*100)/_blockDetails.gasLimit).toStringAsFixed(2)} %',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Uncle hash',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${_blockDetails.uncleHash}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 12.0,
                        fontFamily: 'Monospace',
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Root hash',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${_blockDetails.rootHash}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 12.0,
                        fontFamily: 'Monospace',
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Tx hash',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${_blockDetails.txHash}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 12.0,
                        fontFamily: 'Monospace',
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text('Size',
                      style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      )),
                  new Text(
                    '${_blockDetails.size} bytes',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 19.0,
                        fontWeight: FontWeight.w300),
                  ),
                ],
              ),
              new Padding(padding: const EdgeInsets.all(5.0)),
            ],
          ),
        )
      ],),
    );
  }
}

class BlockDetails extends StatefulWidget {
  BlockDetails({Key key, this.block, this.basicStats}) : super(key: key);

  final Block block;
  final BasicStats basicStats;

  @override
  _BlockDetailsState createState() =>
      new _BlockDetailsState(block, basicStats);
}

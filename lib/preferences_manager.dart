import 'dart:async';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:shared_preferences/shared_preferences.dart';


class PreferencesManager {
  static final PREF_ACCOUNTS = 'accounts';
  static final PREF_THEME = 'theme';

  Future<SharedPreferences> getPreferences() async {
    return SharedPreferences.getInstance();
  }

  Future<int> getTheme() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getInt(PREF_THEME) ?? 0;
  }

  Future<Null> saveTheme(int theme) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt(PREF_THEME, theme);
  }

  Future<Null> saveAccounts(List<Account> accounts) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String accountsCommaSeparated = '';
    accounts.forEach((account) {
      accountsCommaSeparated += account.address + ',';
    });
    sharedPreferences.setString(PREF_ACCOUNTS, accountsCommaSeparated);
  }

  Future<List<Account>> getAccounts() async {
    SharedPreferences sharedPreferences = await getPreferences();
    List<Account> accounts = new List<Account>();
    String accountsCommaSeparated = sharedPreferences.getString(PREF_ACCOUNTS);
    if (accountsCommaSeparated != null) {
      List<String> accountAddresses = accountsCommaSeparated.split(',');
      accountAddresses.removeLast();
      accountAddresses.forEach((accountAddress) {
        accounts.add(new Account(address: accountAddress));
      });
    }
    return accounts;
  }
}

import 'package:etherstats/date_helper.dart';
import 'package:etherstats/etherchain_api.dart';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:etherstats/loading_row.dart';
import 'package:etherstats/transaction_details_page.dart';
import 'package:etherstats/transaction_row.dart';
import 'package:flutter/material.dart';

class _AccountDetailsState extends State<AccountDetails> {
  var _etherChainApi = new EtherChainApi();

  Account _account;
  BasicStats _basicStats;
  List<Transaction> _transactions = new List<Transaction>();

  _AccountDetailsState(account, basicStats) {
    this._account = account;
    this._basicStats = basicStats;

    _fetchAccountAndTransactions();
  }

  void _fetchAccountAndTransactions() {
    _etherChainApi.getAccount(_account.address).then((account) {
      setState(() {
        _account = account;
      });
    });
    _etherChainApi
        .getAccountTransactions(_account.address)
        .then((transactions) {
      setState(() {
        _transactions = transactions;
      });
    });
  }

  void _onTransactionPressed(BuildContext context, transaction) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new TransactionDetails(transaction: transaction, basicStats: _basicStats,);
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Account details"),
      ),
      body: new ListView(
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 2.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Address',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${_account.address}',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 12.0,
                          fontFamily: 'Monospace',
                          fontWeight: FontWeight.w300),
                    )
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Balance',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${(_account.balance/1000000000000000000).toStringAsFixed(8)} Ξ'
                          '\n${((_account.balance/1000000000000000000)*_basicStats.priceUsd).toStringAsFixed(2)} USD',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 19.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('First seen',
                        style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                        )),
                    new Text(
                      '${DateHelper.date(_account.firstSeen)}',
                      style: new TextStyle(
                          color: Colors.black54,
                          fontSize: 19.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
                new Padding(padding: const EdgeInsets.all(5.0)),
                new Card(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                          padding:
                              const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
                          child: new Text('Transactions',
                              style: new TextStyle(
                                color: Colors.black54,
                                fontSize: 20.0,
                                fontWeight: FontWeight.w400,
                              ))),
                      new Padding(padding: const EdgeInsets.all(5.0)),
                      new Column(
                        children: _transactions.length > 0
                            ? _transactions.map((transaction) {
                                return new TransactionRow(
                                    transaction: transaction,
                                    showDirection: true,
                                    received: _account.address == transaction.recipient,
                                    onPressed: (transaction) {
                                      _onTransactionPressed(
                                          context, transaction);
                                    });
                              }).toList()
                            : [new LoadingRow()],
                      ),
                    ],
                  ),
                ),
                new Padding(padding: const EdgeInsets.all(5.0))
              ],
            ),
          )
        ],
      ),
    );
  }
}

class AccountDetails extends StatefulWidget {
  AccountDetails({Key key, this.account, this.basicStats}) : super(key: key);

  final Account account;
  final BasicStats basicStats;

  @override
  _AccountDetailsState createState() => new _AccountDetailsState(account, basicStats);
}

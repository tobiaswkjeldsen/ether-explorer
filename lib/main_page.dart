import 'dart:async';
import 'package:etherstats/about_page.dart';
import 'package:etherstats/account_details_page.dart';
import 'package:etherstats/account_manager.dart';
import 'package:etherstats/block_details_page.dart';
import 'package:etherstats/block_row.dart';
import 'package:etherstats/blockies.dart';
import 'package:etherstats/blocks_page.dart';
import 'package:etherstats/etherchain_api.dart';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:etherstats/preferences_manager.dart';
import 'package:etherstats/transaction_details_page.dart';
import 'package:etherstats/transaction_row.dart';
import 'package:etherstats/transactions_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MainPageState createState() => new _MainPageState();
}

class _MainPageState extends State<MainPage> {
  EtherChainApi _etherChainApi = new EtherChainApi();
  AccountManager _accountManager = new AccountManager();

  TextEditingController _accountAddField = new TextEditingController();

  BasicStats _basicStats = BasicStats.empty();
  List<Account> _accounts = new List<Account>();

  Timer timer;

  _MainPageState() {
    _refreshAccounts();
    _fetchData();
    //timer = new Timer.periodic(new Duration(seconds: 5), _timerCallback);
  }

  void _timerCallback(Timer timer) {
    _fetchData();
  }

  _fetchData() async {
    BasicStats basicStats = await _etherChainApi.getBasicStats();
    setState(() {
      _basicStats = basicStats;
    });
  }

  _refreshAccounts() async {
    List<Account> accounts = await _accountManager.getAccounts();
    setState(() {
      _accounts = accounts;
    });
  }

  void _onTransactionPressed(BuildContext context, transaction) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new TransactionDetails(transaction: transaction, basicStats: _basicStats,);
      },
    ));
  }

  void _onBlockPressed(BuildContext context, block) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new BlockDetails(block: block, basicStats: _basicStats,);
      },
    ));
  }

  void _onTransactionsPressed(BuildContext context) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new TransactionsPage(basicStats: _basicStats,);
      },
    ));
  }

  void _onAccountPressed(BuildContext context, account) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new AccountDetails(account: account, basicStats: _basicStats,);
      },
    ));
  }

  void _onBlocksPressed(BuildContext context) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new BlocksPage(basicStats: _basicStats,);
      },
    ));
  }

  void _onAboutPressed() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new AboutPage();
      },
    ));
  }

  void _onSettingsPressed() {
    Navigator.pushNamed(context, '/settings');
  }

  void _addAccountDialog(context) {
    showDialog<bool>(
        context: context,
        child: new AlertDialog(
            title: const Text("Account address"),
            content: new TextField(
              controller: _accountAddField,
              autofocus: true,
              decoration: const InputDecoration(
                hintText: '0x',
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context, false);
                  }),
              new FlatButton(
                  child: const Text('Add acccount'),
                  onPressed: () {
                    _addAccount(_accountAddField.text);
                    Navigator.pop(context, false);
                  }),
            ]));
  }

  Future _addAccount(accountAddress) async {
    await _accountManager.addAccount(accountAddress);
    _refreshAccounts();
  }

  void _removeAccountDialog(context, Account account) {
    showDialog<bool>(
        context: context,
        child: new AlertDialog(
            title: const Text("Remove account"),
            content: new Text('Do you want to remove ${account.address}?'),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context, false);
                  }),
              new FlatButton(
                  child: const Text('Remove'),
                  onPressed: () {
                    _removeAccount(account);
                    Navigator.pop(context, false);
                  }),
            ]));
  }

  Future _removeAccount(Account account) async {
    await _accountManager.removeAccount(account.address);
    _refreshAccounts();
  }

  Column _buildAccountListTiles(context) {
    List<ListTile> accountListTiles = new List<ListTile>();
    accountListTiles.add(new ListTile(
      leading: const Icon(Icons.add),
      title: const Text('Add account'),
      onTap: () => _addAccountDialog(context),
    ));
    _accounts.forEach((account) {
      accountListTiles.add(new ListTile(
        leading: new Image.network('http://identicon.org?t=${account.address}&s=64'),
        title: new Text(account.address),
        onTap: () => _onAccountPressed(context, account),
        onLongPress: () => _removeAccountDialog(context, account),
      ));
    });
    return new Column(children: accountListTiles);
  }

  Widget _buildDrawer(BuildContext context) {
    return new Drawer(
      child: new ListView(
        children: <Widget>[
          new DrawerHeader(
              child: new Center(
                  child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new ImageIcon(
                new AssetImage('images/ether.png'),
                size: 100.0,
                color: Colors.grey,
              ),
            ],
          ))),
          _buildAccountListTiles(context),
          const Divider(),
          new ListTile(
            title: const Text('Transactions'),
            onTap: () => _onTransactionsPressed(context),
          ),
          new ListTile(
            title: const Text('Blocks'),
            onTap: () => _onBlocksPressed(context),
          ),
          const Divider(),
          new ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: _onSettingsPressed,
          ),
          new ListTile(
            leading: const Icon(Icons.help),
            title: const Text('About'),
            onTap: _onAboutPressed,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Row(
          children: <Widget>[
            new ImageIcon(new AssetImage('images/ether.png')),
            new Text(widget.title)
          ],
        ),
      ),
      body: new RefreshIndicator(
          onRefresh: _fetchData,
          child: new ListView(
            children: <Widget>[
              new Container(
                  padding: const EdgeInsets.fromLTRB(5.0, 8.0, 5.0, 2.0),
                  child: new Card(
                      child: new Column(
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Container(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 16.0, 0.0, 16.0),
                              child: new Column(
                                children: <Widget>[
                                  new Text('Difficulty',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w400,
                                      )),
                                  new Text('${_basicStats.difficulty}',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w300,
                                      ))
                                ],
                              )),
                          new Container(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 16.0, 0.0, 16.0),
                              child: new Column(
                                children: <Widget>[
                                  new Text('New block',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w400,
                                      )),
                                  new Text('${_basicStats.blockTime}',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w300,
                                      ))
                                ],
                              )),
                          new Container(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 16.0, 0.0, 16.0),
                              child: new Column(
                                children: <Widget>[
                                  new Text('Uncle rate',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w400,
                                      )),
                                  new Text('${_basicStats.uncleRatePercent}',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w300,
                                      ))
                                ],
                              )),
                        ],
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Container(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 0.0, 0.0, 16.0),
                              child: new Column(
                                children: <Widget>[
                                  new Text('Hash rate',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w400,
                                      )),
                                  new Text('${_basicStats.hashRate}',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w300,
                                      ))
                                ],
                              )),
                          new Container(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 0.0, 0.0, 16.0),
                              child: new Column(
                                children: <Widget>[
                                  new Text('USD',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w400,
                                      )),
                                  new Text('${_basicStats.priceUsd}',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w300,
                                      ))
                                ],
                              )),
                          new Container(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 0.0, 0.0, 16.0),
                              child: new Column(
                                children: <Widget>[
                                  new Text('BTC',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w400,
                                      )),
                                  new Text('${_basicStats.priceBtc}',
                                      style: new TextStyle(
                                        color: Colors.black54,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w300,
                                      ))
                                ],
                              )),
                        ],
                      ),
                    ],
                  ))),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                      padding: const EdgeInsets.fromLTRB(16.0, 10.0, 16.0, 0.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Text('Latest transactions',
                              style: new TextStyle(
                                color: Colors.black54,
                                fontSize: 20.0,
                                fontWeight: FontWeight.w400,
                              )),
                          new FlatButton(
                              onPressed: () {
                                _onTransactionsPressed(context);
                              },
                              child: new Text('View more'))
                        ],
                      )),
                  new Padding(padding: const EdgeInsets.all(5.0)),
                  new Column(
                    children: _basicStats.transactions.map((transaction) {
                      return new TransactionRow(
                          transaction: transaction,
                          onPressed: (transaction) {
                            _onTransactionPressed(context, transaction);
                          });
                    }).toList(),
                  )
                ],
              ),
              new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        padding:
                            const EdgeInsets.fromLTRB(16.0, 10.0, 16.0, 0.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Text('Latest blocks',
                                style: new TextStyle(
                                  color: Colors.black54,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w400,
                                )),
                            new FlatButton(
                                onPressed: () {
                                  _onBlocksPressed(context);
                                },
                                child: new Text('View more'))
                          ],
                        )),
                    new Padding(padding: const EdgeInsets.all(5.0)),
                    new Column(
                      children: _basicStats.blocks.map((block) {
                        return new BlockRow(
                            block: block,
                            onPressed: (block) {
                              _onBlockPressed(context, block);
                            });
                      }).toList(),
                    )
                  ]),
            ],
          )),
      drawer: _buildDrawer(context),
    );
  }
}

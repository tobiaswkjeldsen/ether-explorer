import 'package:etherstats/date_helper.dart';
import 'package:etherstats/etherchain_assembler.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

typedef void BlockRowActionCallback(Block block);

class BlockRow extends StatelessWidget {
  final Block block;
  final BlockRowActionCallback onPressed;

  BlockRow({Key key, this.block, this.onPressed}) : super(key: key);

  static const double kHeight = 60.0;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
        onTap: () {
          onPressed(block);
        },
        child: new Container(
            decoration: new BoxDecoration(
                border: new Border(
                    bottom:
                        new BorderSide(color: Theme.of(context).dividerColor))),
            padding: const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
            child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  new Text(
                    '#${block.number}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 15.0,
                        fontWeight: FontWeight.w300),
                  ),
                  new Text(
                    '${block.coinBase.substring(0,10)}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 12.0,
                        fontFamily: 'Monospace',
                        fontWeight: FontWeight.w300),
                  ),
                  new Text(
                    '${DateHelper.minutesOrHoursOrDaysAgo(block.time)}',
                    style: new TextStyle(
                        color: Colors.black54,
                        fontSize: 15.0,
                        fontWeight: FontWeight.w300),
                  ),
                ])));
  }
}

import 'package:etherstats/etherchain_assembler.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

typedef void TransactionRowActionCallback(Transaction transaction);

class LoadingRow extends StatelessWidget {
  LoadingRow({Key key}) : super(key: key);

  static const double kHeight = 60.0;

  @override
  Widget build(BuildContext context) {
    return new Container(
        decoration: new BoxDecoration(
            border: new Border(
                bottom: new BorderSide(color: Theme.of(context).dividerColor))),
        padding: const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
        child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new CircularProgressIndicator(
                value: null,
              )
            ]));
  }
}

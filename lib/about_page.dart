import 'dart:async';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class _AboutPageState extends State<AboutPage> {

  Future _launchEtherchain(url) async {
    var url = 'https://etherchain.org/account/0x004cfeee04C4F3957B53421C16212f6963979Df8';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("About")),
      body: new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              title: const Text('Ether Explorer'),
              subtitle: const Text('Explore the Ethereum blockchain'),
            ),
            new ListTile(
              title: const Text('Developer'),
              subtitle: const Text('Tobias Westergaard Kjeldsen <tobias@wkjeldsen.dk>\n0x004cfeee04C4F3957B53421C16212f6963979Df8'),
              onTap: () {_launchEtherchain('https://etherchain.org');},
            ),
            new ListTile(
              title: const Text('Made possible by the etherchain.org API'),
              subtitle: const Text('https://etherchain.org'),
              onTap: () {_launchEtherchain('https://etherchain.org');},
            ),
          ],
        ),
      )
      ,
    );
  }
}

class AboutPage extends StatefulWidget {
  AboutPage({Key key}) : super(key: key);

  @override
  _AboutPageState createState() => new _AboutPageState();
}

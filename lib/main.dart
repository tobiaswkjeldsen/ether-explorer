import 'package:etherstats/etherchain_api.dart';
import 'package:etherstats/main_page.dart';
import 'package:etherstats/preferences_manager.dart';
import 'package:etherstats/setting_types.dart';
import 'package:etherstats/settings_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => new MyAppState();
}

class MyAppState extends State<MyApp> {

  var preferencesManager = new PreferencesManager();

  MyAppState() {
    preferencesManager.getTheme().then((themeColor){
      configurationUpdater(_configuration.copyWith(theme: ThemeColor.values[themeColor]));
    });
  }

  Setting _configuration = new Setting(
    theme: ThemeColor.blue,
  );

  void configurationUpdater(Setting value) {
    setState(() {
      _configuration = value;
    });
  }

  ThemeData get theme {
    switch(_configuration.theme) {
      case ThemeColor.blueGrey:
        return new ThemeData(
          primarySwatch: Colors.blueGrey,
        );
      case ThemeColor.grey:
        return new ThemeData(
          primarySwatch: Colors.grey,
        );
      case ThemeColor.green:
        return new ThemeData(
          primarySwatch: Colors.green,
        );
      case ThemeColor.red:
        return new ThemeData(
          primarySwatch: Colors.red,
        );
      case ThemeColor.blue:
        return new ThemeData(
          primarySwatch: Colors.blue,
        );
      case ThemeColor.purple:
        return new ThemeData(
          primarySwatch: Colors.purple,
        );
    }
    return new ThemeData(
      primarySwatch: Colors.blue,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Ether Explorer',
      theme: theme,
      home: new MainPage(title: 'Ether Explorer'),
      routes: <String, WidgetBuilder>{
        '/settings': (BuildContext context) => new Settings(_configuration, configurationUpdater)
      },
    );
  }
}
